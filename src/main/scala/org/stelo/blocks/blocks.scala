package org.stelo.blocks

import scala.language.implicitConversions

trait Block {
	def home: Int
	def label: Any
	override def equals(that: Any) = that match {
		case that: Block => that.label == this.label
		case _ => false
	}

	override def toString = label.toString
}

//implicit def intToBlock(label: Int) = new BlockInt(label)

object Block {
	def apply(label: Int): Block = new BlockInt(label)

	def stack(blocks: Int*) =
		new BlockStack(blocks.map { Block(_) })

	def stacks(stacks: BlockStack*) =
		new Blocks(stacks)

	def initial(size: Int) =
		new Blocks(Nil).putHomeAll(
			new BlockStack((0 until size).map(Block(_)))
		)

	implicit def intToBlock(i: Int) = Block(i)

}

class BlockInt(n: Int) extends Block {
	def label = n
	def home  = label
}

class BlockStack(private val contents: Seq[Block]) {

	def top = contents.head

	def height = contents.length

	def chop(b: Block) =
		new BlockStack(contents.slice(contents.indexOf(b) + 1, contents.length))

	def above(b: Block): BlockStack =
		if (contents.head == b  ||
			!contents.contains(b)) new BlockStack(Nil)
		else remove.above(b).add(contents.head)

	def atAndAbove(b: Block): BlockStack =
		new BlockStack(Seq(b)).addAll(above(b))

	def freeUp(b: Block): BlockStack =
		if (contents.length < 1 ||
			contents.head == b) this
		else remove.freeUp(b)
	

	def contains(bnew: Block) = contents.contains(bnew)

	def add(bnew: Block) = new BlockStack(bnew +: contents)
	def addAll(stk: BlockStack) = new BlockStack(stk.contents ++ contents)

	def remove = new BlockStack(contents.tail)

	override def equals(x: Any) = x match {
		case b: BlockStack => b.contents.sameElements(contents)
		case _ => false
	}

	override def toString = contents.reverse.mkString(" ")
}
	
class Blocks(private val stacks: Seq[BlockStack]) {

	def above(b: Block, inc: Boolean = false) = {
		val stk = stacks.find(_.contains(b)).get
		if (inc) stk.atAndAbove(b) else stk.above(b)
	}

	def freeUp(b: Block, remove: Boolean = false) =
		(update(b) { (stk: BlockStack) =>
			val next = stk.freeUp(b);
			if (remove) next.remove
			else next
		}).putHomeAll(above(b))

	def removePile(b: Block) =
		update(b) { _.chop(b) }

	def remove(b: Block) = freeUp(b, true)

	def size = stacks.length

	// returns a new set of blocks which is the same as this one,
	// except for pile matching 'which', which will be passed to the
	// callback and the return value will be inserted instead
	
	def update(which: Int)(cb: (BlockStack) => BlockStack) =
		new Blocks(stacks.zipWithIndex.map(
			(tup: (BlockStack, Int)) => 
			if(tup._2 == which) cb(tup._1) else tup._1
		))
		
	def findBlock(b: Block) = stacks.indexWhere(_.contains(b))

	def update(b: Block)(cb: (BlockStack) => BlockStack): Blocks =
		update(findBlock(b))(cb)

	def ensureSize(n: Int) =
		new Blocks(stacks.padTo(n, new BlockStack(Nil)))

	def put(b: Block, where: Int) =
		ensureSize(where + 1)
			.update(where) { (stk: BlockStack) => stk.add(b) }

	def putOnto(a: Block, b: Block) =
		put(a, findBlock(b))

	def putStack(stk: BlockStack, where: Int) =
		ensureSize(where + 1)
			.update(where) { (dst: BlockStack) => dst.addAll(stk) }

	def putHome(b: Block) = put(b, b.home)

	def putHomeAll(b: BlockStack): Blocks =
		if (b.height == 0) this
		else putHome(b.top).putHomeAll(b.remove)

	override def equals(x: Any) = x match {
		case b: Blocks => b.stacks.sameElements(stacks)
		case _ => false
	}

	override def toString =
		stacks.zipWithIndex.map(
			(tup: (BlockStack, Int)) =>
			tup._2 + ": " + tup._1.toString).mkString("\n")

}
