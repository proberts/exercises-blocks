package org.stelo.blocks

import java.io.FileInputStream
import scala.io.BufferedSource

trait RobotCommand {

	def name: String

	def execute(blocks: Blocks): Blocks

	// if this returns true, then this command indicates the end of
	// the program
	def end = false

	override def toString = name

}

class Robot(var state: Blocks) {
	def tick(s: String) =
		state = makeCmd(s).execute(state)

	def makeCmd(s: String): RobotCommand = s.split(' ') match {
		case Array("move", a, "onto", b) =>
			new CmdMoveOnto(Block(a.toInt), Block(b.toInt))
		case Array("move", a, "over", b) =>
			new CmdMoveOver(Block(a.toInt), Block(b.toInt))
		case Array("pile", a, "onto", b) =>
			new CmdPileOnto(Block(a.toInt), Block(b.toInt))
		case Array("pile", a, "over", b) =>
			new CmdPileOver(Block(a.toInt), Block(b.toInt))
		case Array("quit") => new CmdQuit
		case _ => CmdNoop
	}

	override def toString = state.toString

}

object RobotApp {

	def main(args: Array[String]) = {
		for (fname <- args) {
			val input = new BufferedSource(new FileInputStream(fname)).getLines
			if (input.hasNext) {
				val n  = input.next.toInt
				val r  = new Robot(Block.initial(n))
				for (line <- input) {
					r.tick(line)
				}
				println(r.state)
			}
		}
	}
}

object CmdNoop extends RobotCommand {
	def name = "[NOOP]"
	def execute(blocks: Blocks) = blocks
}

class CmdMoveOnto(a: Block, b: Block) extends RobotCommand {
	def name = "move [" + a+ "] onto [" + b + "]"

	def execute(blocks: Blocks) =
		blocks.remove(a).freeUp(b).putOnto(a, b)

}

class CmdMoveOver(a: Block, b: Block) extends RobotCommand {
	def name = "move [" + a + "] onto [" + b + "]"

	def execute(blocks: Blocks) =
		blocks.remove(a).putOnto(a, b)
}

class CmdPileOnto(a: Block, b: Block) extends RobotCommand {
	def name = "pile [" + a + "] onto [" + b + "]"

	def execute(blocks: Blocks) =
		blocks.removePile(a)
			.freeUp(b)
			.putStack(blocks.above(a, true), blocks.findBlock(b))
}

class CmdPileOver(a: Block, b: Block) extends RobotCommand {
	def name = "pile [" + a + "] over [" + b + "]"

	def execute(blocks: Blocks) =
		blocks.removePile(a)
			.putStack(blocks.above(a, true), blocks.findBlock(b))
}

class CmdQuit extends RobotCommand {
	def name = "quit"

	def execute(blocks: Blocks) = blocks

	override def end = true

}
