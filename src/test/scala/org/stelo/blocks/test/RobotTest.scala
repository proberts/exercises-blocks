import org.scalatest._
import org.stelo.blocks.Block.{stack,stacks}
import org.stelo.blocks._

// class BlocksMutable(private var b: Blocks) {

// 	var step = 1

// 	def $(cb: (Blocks) => Blocks) = {
// 		b = cb(b)
// 		print("After step " + step + ": =>\n" + toString)
// 		step += 1
// 	}

// 	def $$(cb: (Blocks) => Any) =
// 		print(cb(b))

// 	override def equals(x: Any) = x match {
// 		case x: Blocks => b == x
// 		case x: BlocksMutable => b == x.b
// 		case _ => false
// 	}

// 	override def toString = b.toString

// }

class RobotTest extends FlatSpec {

	"Pile [3] Onto [5]" should "move blocks above 3 onto 5 with removal" in {
		val start = stacks(
			stack(1, 7, 3, 6),
			stack(2, 4, 5)
		)
		val end   = stacks(
			stack(6),
			stack(1, 7, 3, 5),
			stack(2),
			stack(),
			stack(4))

		println("Before: " + start)

		val cmd   = new CmdPileOnto(Block(3), Block(5))

		assert(cmd.execute(start) == end)
	}

	"Pile [3] Over [5]" should "move blocks above 3 onto 5 without removal" in {
		val start = stacks(
			stack(1,2,3,4),
			stack(7, 6, 5))
		val end = stacks(
			stack(4),
			stack(1, 2, 3, 7, 6, 5))

		val cmd = new CmdPileOver(Block(3), Block(5))
		assert(cmd.execute(start) == end)

	}
}
